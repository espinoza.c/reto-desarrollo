require('dotenv').config();
const Koa = require('koa');
const koaBody = require('koa-body');
const json = require('koa-json');
const mongoose = require('mongoose');
const cors = require('koa-cors');


const {getAllRouter, findRouter, findReplaceRouter} = require('./api');
const swagger = require("swagger2");
const { ui, validate } = require("swagger2-koa");
const swaggerDocument = swagger.loadDocumentSync("api.yaml");

const logger = require('koa-logger');



const app = new Koa();

const PORT = 3000

mongoose.connect(`mongodb://${process.env.DB_SERVER}:${process.env.DB_PORT}/${process.env.DB_NAME}`);

mongoose.connection.on('error', (err) => {
  console.log(err);
});

//CORS middleware
app.use(cors());

//Logger middleware
app.use(logger())

//Middleware to parse the body of the request
app.use(koaBody());

//Route middleware
app.use(getAllRouter.middleware())
app.use(findRouter.middleware())
app.use(findReplaceRouter.middleware())




//Route Swagger
app.use(ui(swaggerDocument, "/swagger"))

//Middleware to make the json prettier
app.use(json());

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});

