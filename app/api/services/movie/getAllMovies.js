require('dotenv').config();
const Movie = require("../../../models/Movie");

const getAllMovies = async (ctx) => {
  const page = ctx.request.headers.page;
  const limit = process.env.LIMIT_PAGE;
  try {
    const movies = await Movie.find().limit(limit).skip(limit * (page - 1));
    const count = await Movie.countDocuments();
    ctx.body = { movies, totalPages: Math.ceil(count / limit), currentPage: page };
  } catch (err) {
    ctx.body = { message: err };
  }
};

module.exports = getAllMovies;
