require('dotenv').config();

const Movie = require("../../../models/Movie");
const axios = require("axios");


const findMovie = async (ctx) => {
  
  if (ctx.invalid) {
    console.log(ctx.invalid.query);
    ctx.body = await render('add', { errors: ctx.invalid });
  }
  try {
    let movieFound = {};
    const movieTitle  = ctx.query.movie;
    const movieYear = ctx.request.headers.year;
    if (!movieTitle) {
      ctx.status = 409;
      ctx.body = {
        message: "Movie name is required",
      };
    }

    let url = `${process.env.OMDB_API}/?apikey=${process.env.API_KEY}&t=${movieTitle}&y=${movieYear}`;
    

    await axios.get(url).then((res) => {
      movieFound = res.data;
    });

    const movie = await Movie.findOne({ title: movieFound.Title });
    if (!movie) {
      const newMovie = new Movie({
        title: movieFound.Title,
        year: movieFound.Year,
        released: movieFound.Released,
        plot: movieFound.Plot,
        actors: movieFound.Actors,
        genre: movieFound.Genre,
        director: movieFound.Director,
        ratings: movieFound.Ratings.concat.apply(movieFound.Ratings.map((el) => el.Value + " , " + el.Source)),
      });
      newMovie.save();
      ctx.body = newMovie;
    } else {
      ctx.body = movie;
    }
  } catch (error) {
    ctx.status = 409;
    ctx.body = error;
  }
};

module.exports = findMovie;
