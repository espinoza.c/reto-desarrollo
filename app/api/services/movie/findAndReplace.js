const Movie = require("../../../models/Movie");

const findAndReplace = async (ctx) => {
  const data = ctx.request.body;
  try {
    let plotUpdated = ''
      await Movie.findOne({ title: data.movie}).then((movie) => {
        plotUpdated = movie.plot.replaceAll(data.find, data.replace);
      })
    await Movie.findOneAndUpdate({ title: data.movie }, {plot: plotUpdated}).then((movie) => {
      ctx.body = movie.plot.replaceAll(data.find, data.replace);
    });
  } catch (err) {
    console.log(err);
    ctx.status = 409;
    ctx.body = err;
  }
};

module.exports = findAndReplace;

