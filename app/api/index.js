const koa = require('koa');
const router = require('koa-joi-router');

const getMovieByName = require('./services/movie/findMovie')
const getAll = require('./services/movie/getAllMovies')
const postAndReplace = require('./services/movie/findAndReplace')
const movieJoiSchema = require('../models/movieSchema');

const Joi = router.Joi;
const getAllRouter = router();
const findRouter = router();
const findReplaceRouter = router();

findRouter.route({
  method: 'get',
  path: '/movie/find',
  validate: {
    query:{
      movie: Joi.string().required()
    },
    headers:{
      year: Joi.number()
    },
    output: {
      200: {
        body: Object,
      }
    }
  },
  handler: getMovieByName
});

getAllRouter.route({
  method: 'get',
  path: '/movie/getAll',
  validate: {
    headers:{
      page: Joi.number().required()
    },
    output: {
      200: {
        body:{
          movies:Joi.array().items(Object).required(),
          totalPages: Joi.number(),
          currentPage:Joi.number()
        }
      }
    }
  },
  handler: getAll
});

findReplaceRouter.route({
  method: 'post',
  path: '/movie/replace',
  validate: {
    body: {
      movie: Joi.string().required(),
      find: Joi.string().required(),
      replace: Joi.string().required(),
    },
    type:'json',
    output: {
      200: {
        body: Joi.string().required(),
      }
    }
  },
  handler: postAndReplace
});



module.exports = {getAllRouter, findRouter, findReplaceRouter};