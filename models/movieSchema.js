const router = require('koa-joi-router');
const Joi = router.Joi;

const movieJoiSchema = Joi.object().keys({
    _id: Joi.string().required(),
    title: Joi.string().required(),
    year: Joi.number().required(),
    released: Joi.string().required(),
    genre: Joi.string().required(),
    actors: Joi.string().required(),
    director: Joi.string().required(),
    plot: Joi.string().required(),
    ratings: Joi.array().required(),
    createdAt:Joi.string(),
    __v:Joi.number()
  });

module.exports = movieJoiSchema